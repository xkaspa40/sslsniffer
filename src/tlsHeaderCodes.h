//codes for TLS messages
#define TLS_HANDSHAKE 0x16
#define TLS_CHANGE_CIPHER_SPEC 0x14
#define TLS_APP_DATA 0x17
#define TLS_ALERT 0x15
#define TLS_CLIENT_HELLO 0x01
#define TLS_SERVER_HELLO 0x02

//lengths of some of the TLS sections
#define HANDSHAKE_LENGHT_BYTES 3
#define HANDSHAKE_VERSION_BYTES 2
#define HANDSHAKE_RANDOM_BYTES 32
#define EXTENSIONS_LENGTH_BYTES 2
#define SNI_LIST_LEN_BYTES 2
#define SNI_TYPE_BYTES 1
#define EXT_TYPE_BYTES 2

//TLS versions
#define TLSv1_0 0x0301
#define TLSv1_1 0x0302
#define TLSv1_2 0x0303
#define TLSv1_3 0x0304

//resrved code of Server Name Indication extension
#define EXT_SNI_CODE 0x0000

//TCP states
typedef enum {
    SYN,
    SYN_ACK,
    ACK,
    FIN,
    FIN2,
    CLOSED
} NEXTSTATE;

//TLS states
typedef enum {
    CLIENT_HELLO,
    SERVER_HELLO,
    APP_DATA
} NEXTTLS;

//structure for storing connection-related data
struct SnifferStruct
{
    u_int srcPort;
    char *srcIp;
    u_int dstPort;
    char *dstIp;
    
    int epochStartCommSec;
    int epochStartCommUSec;
    int epochEndCommSec;
    int epochEndCommUSec;
    
    char *sni;
    int packets;
    int bytes;

    NEXTSTATE nextState;
    NEXTTLS nextTls;

    struct SnifferStruct *next;
} ;

struct SnifferStruct* getSslNode(u_int srcPort, u_int dstPort, char *srcIp, char *dstIp);
struct SnifferStruct* getLastNode();
void freeStructs();
void insertSslNode (struct SnifferStruct *new);
void removeNode(struct SnifferStruct *node);
void initializeSslStruct();
bool isSameConnection(u_int srcPort, u_int dstPort, char *srcIp, char *dstIp, struct SnifferStruct *node);
void writeSni(u_char *data, int extLength, struct SnifferStruct *node);
void printLine(struct SnifferStruct *node);
int getNextTcpFlag(const struct tcphdr *tcpHeader, int actualState);
int getNextTlsFlag(u_char *data, int actualState);
float addZerosBeforeUSec(int uSeconds);
struct SnifferStruct *sslStructs;