#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <net/ethernet.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <pcap.h>
#include <time.h>
#include "tlsHeaderCodes.h"

// global helpers for debugging
int packet_counter = 0;
int structCount = 0;

/**
 * Prints help and quits program.
 */
void printHelp() 
{
    printf("Usage: ./sslsniff [-r filename] [-i interface]\nBoth paremeters at once aren't supported. Sudo is needed for interface listening.\n");
    
    exit(1);
}

/**
 * Handles input arguments and call printHelp() on error.
 * 
 * @param argc 		 Count of arguments
 * @param argv 		 Array of arguments
 * @param filename	 Will be filled with file name given with -r argument
 * @param interface  Will be filled with interface name given with -i argument
 * @param set		 Indicates whether an argument(s) were set			
 * 
 * @return void
 */
void handleArguments(int argc, char *argv[], char **fileName, char **interface, bool* set) 
{
    int option;

    while ((option = getopt(argc, argv, "r:i:")) != -1) {
            if (*set) {
                printHelp();
            }

            switch (option) {
                case 'r': 
                    *set = true;
                    *fileName = optarg;
                    break;
                case 'i':
                    *set = true;
                    *interface = optarg;
                    break;
            }
    }

    if ( ! *set) {
        printHelp();
    }

    return;
}

/**
 * Pretty much all logic of this tool is in this function. It first gets source and dest IPs and ports
 * by moving packet pointer by ethernet header, resp. IP header length. Then we check if the used protocol is TCP.
 * After we are sure it's woth examinating this packet we look if we already have some info (have SnifferStruct node) 
 * about this packer. If not we make new node, fill it with all needed info and insert it. The rest is just going byte by byte
 * through the TCP payload looking for TLS data.
 * 
 * @param userData 
 * @param pkthdr 
 * @param packet    packet data
 * 
 */
void pcapHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet) 
{
    //incrementing for debugging purpose
    packet_counter++;
    //ethernet header struct
    const struct ether_header* ethernetHeader;
    //separate IPv4 and IPv6 structures
    const struct iphdr* ipHeader;
    const struct ip6_hdr* ip6Header;
    //tcp header struct
    const struct tcphdr* tcpHeader;
    //separate source and dest IPs for v4 and v6
    char sourceIP[INET_ADDRSTRLEN];
    char destIP[INET_ADDRSTRLEN];
    char sourceIP6[INET6_ADDRSTRLEN];
    char destIP6[INET6_ADDRSTRLEN];
    u_int sourcePort, destPort;
    // pointer used to go though the packet
    u_char *data;
    // helpers for reading two bytes at once
    u_char* first;
    u_char* second;

    bool isIpv6;
    // used for payload length so we don't get out of memory boundaries
    int dataLength = 0;

    //getting ethernet header and ip version
    ethernetHeader = (struct ether_header*)packet;
    if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP){
        isIpv6 = false;
    } else if ( ntohs(ethernetHeader->ether_type) == ETHERTYPE_IPV6) {
        isIpv6 = true;
    } else {
        return;
    }
    
    //getting IP address according to version (4 or 6)
    ip6Header = (struct ip6_hdr*)(packet + sizeof(struct ether_header));
    ipHeader = (struct iphdr*)(packet + sizeof(struct ether_header));
    if (isIpv6) {
        inet_ntop(AF_INET6, &(ip6Header->ip6_src), sourceIP6, INET6_ADDRSTRLEN);
        inet_ntop(AF_INET6, &(ip6Header->ip6_dst), destIP6, INET6_ADDRSTRLEN);
    } else {
        inet_ntop(AF_INET, &(ipHeader->saddr), sourceIP, INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(ipHeader->daddr), destIP, INET_ADDRSTRLEN);
    }

    //getting TCP header
    if (isIpv6) {
        if (ip6Header->ip6_ctlun.ip6_un1.ip6_un1_nxt != IPPROTO_TCP) {
            return;
        }

        tcpHeader = (struct tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip6_hdr));
    } else {
         if (ipHeader->protocol != IPPROTO_TCP) {
            return;
        }
        
        tcpHeader = (struct tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct iphdr));
    }
    //ports
    sourcePort = htons(tcpHeader->source);
    destPort = htons(tcpHeader->dest);
    
    //Gettint SnifferStruct node (if we already have this connection) or NULL (for new connection)
    struct SnifferStruct* node;
    if (isIpv6) {
        node = getSslNode(sourcePort, destPort, sourceIP6, destIP6);
    } else {
        node = getSslNode(sourcePort, destPort, sourceIP, destIP);
    }
    
    
    // new tcp connection detected without SYN as first message so whole packet is skipped
    if ( ! (tcpHeader->syn == 1 && tcpHeader->ack == 0) && node == NULL) {
        return;
    }
    
    //creating new SnifferStruct node for new connection
    if (node == NULL) {
        node = malloc(sizeof(struct SnifferStruct));
        node->srcPort = sourcePort;
        node->dstPort = destPort;
        if (isIpv6) {
            node->srcIp = malloc(INET6_ADDRSTRLEN * sizeof(char));
            memcpy(node->srcIp, sourceIP6, INET6_ADDRSTRLEN);
            node->dstIp = malloc(INET6_ADDRSTRLEN * sizeof(char));
            memcpy(node->dstIp, destIP6, INET6_ADDRSTRLEN);
        } else {
            node->srcIp = malloc(INET_ADDRSTRLEN * sizeof(char));
            memcpy(node->srcIp, sourceIP, INET_ADDRSTRLEN);
            node->dstIp = malloc(INET_ADDRSTRLEN * sizeof(char));
            memcpy(node->dstIp, destIP, INET_ADDRSTRLEN);
        }
        node->nextState = SYN;
        node->nextTls = CLIENT_HELLO;
        node->bytes = 0;
        node->packets = 0;
        node->epochStartCommSec = pkthdr->ts.tv_sec;
        node->epochStartCommUSec = pkthdr->ts.tv_usec;
        insertSslNode(node);
    }
   //getting anticipated TCP flag so we know when handshake is finished or connection is terminated
    node->nextState = getNextTcpFlag(tcpHeader, node->nextState);
    node->packets++;

    //tcp closed connection
    if (node->nextState == CLOSED) {
        //that also completed tls handshake
        if (node->nextTls == APP_DATA) {
            node->epochEndCommSec = pkthdr->ts.tv_sec;
            node->epochEndCommUSec = pkthdr->ts.tv_usec;
            printLine(node);
        }
        //removing closed connection
        removeNode(node);
        return;
    }

     //getting lengths so we can get to payload
    int ipHeaderLength = isIpv6 ? sizeof(struct ip6_hdr) : sizeof(struct ip);
    int tcpHeaderLength = sizeof(tcpHeader);
    //length of payload
    dataLength = pkthdr->caplen - (sizeof(struct ether_header) + ipHeaderLength + tcpHeaderLength);
    //pointer to the top of payload
    data = (u_char*)(packet + sizeof(struct ether_header) + ipHeaderLength + tcpHeaderLength);


    //look for version in this packet?
    bool findVersion = false;
    //look for TLS length in this packet?
    bool getLength = false;
    //look for SNI packet?
    bool getSni = false;
    //is TLS handshake?
    bool isHandshake = false;
    //look for handshake type in this packet?
    bool getHandshakeType = false;
    //iterator
    int i = 0;
    short version;
    short length;
    while (i < dataLength) {
        if (*data == TLS_CHANGE_CIPHER_SPEC || *data == TLS_ALERT || *data == TLS_HANDSHAKE || *data == TLS_APP_DATA) {
            findVersion = true;
            if (*data == TLS_HANDSHAKE) {
                isHandshake = true;
            }

            //not enough space in this packet
            if (i + 2 > dataLength) {
                return;
            }
            //get next two bytes
            first = ++data;
            second = ++data;
            version = (*(first)<<8)|*(second);
            i += 2;
            continue;
        }

        if (findVersion) {
            findVersion = false;
            if (version == TLSv1_0 || version == TLSv1_1 || version == TLSv1_2 || version == TLSv1_3) {
                getLength = true;
                //not enough space in this packet
                if (i + 2 > dataLength) {
                    return;
                }
                //next two
                first = ++data;
                second = ++data;
                length = (*(first)<<8)|*(second);
                i += 2;
                continue;
            }
        }

        if (getLength) {
            getLength = false;
            node->bytes += length;

            if (isHandshake) {
                isHandshake = false;
                getHandshakeType = true;
                data++;
                i++;
                continue;
            } 
        }

        if (getHandshakeType) {
            getHandshakeType = false;
            //getting handshake type
            NEXTTLS actual = node->nextTls;
            node->nextTls = getNextTlsFlag(data, node->nextTls);
            //anticipating server hello and having client hello means we should look for SNI right in this packet
            if (node->nextTls == SERVER_HELLO && actual == CLIENT_HELLO) {
                getSni = true;
                break;
            }
        }
        data++;
        i++;
    }

    //SNI is NOT in this packet
    if ( ! getSni) {
        return;
    }

    //getting the position of session ID byte
    data += HANDSHAKE_LENGHT_BYTES + HANDSHAKE_VERSION_BYTES + HANDSHAKE_RANDOM_BYTES + 1;
    //skipping the session ID
    int sid = (*data);
    data += sid;
    //skipping cipher suits
    first = ++data;
    second = ++data;
    data += (*(first)<<8)|*(second);
    //getting compression methods length
    data++;
    //skipping compression methods
    data += (*data);
    //getting extensions length
    first = ++data;
    second = ++data;
    int extLength = (*(first)<<8)|*(second);
    //writes SNI in node
    writeSni(data, extLength, node);
}

/**
 * Prints out line with connection details
 * 
 * @param node  node containing connection we want to print out
 */
void printLine(struct SnifferStruct *node) {
    //format epoch start to human readeable
    time_t start = node->epochStartCommSec;
    char buf[80];
    struct tm  ts;
    ts = *localtime(&start);
    strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", &ts);
    printf("%s.%d,", buf, node->epochStartCommUSec);
    printf("%s,%d,", node->srcIp, node->srcPort);
    printf("%s,%s,", node->dstIp, node->sni);
    printf("%d,%d,", node->bytes, node->packets);

    //get duration time
    int durationSec = node->epochEndCommSec - node->epochStartCommSec;
    int durationU = node->epochEndCommUSec - node->epochStartCommUSec;
    float duration = durationSec + addZerosBeforeUSec(durationU);
    printf("%f\n", duration);
}

/**
 * Epoch nanoseconds to seconds 
 */
float addZerosBeforeUSec(int uSeconds) {
    if (uSeconds == 0) {
        return uSeconds;
    }

    return (float) uSeconds / 1000000;
}

/**
 * Gets SNI from tls extensions and writes it in SnifferStruct node
 * 
 * @param data          pointer to start of TLS extensions
 * @param extLengths    length of TLS extensions to watch for memory boundaries
 * @param node          SnifferStruct node to write the SNI in
 */
void writeSni(u_char *data, int extLengths, struct SnifferStruct *node) {
    int i = 0;
    u_char* first;
    u_char* second;
    //iterating over extensions
    while (i < extLengths) {
        //getting two bytes of extenion type
        first = ++data;
        second = ++data;
        int extType = (*(first)<<8)|*(second);
        i += 2;

        //getting two bytes of this particular extension
        first = ++data;
        second = ++data;
        int length = (*(first)<<8)|*(second);
        i += 2;
        //continue if extension type isn't Server Name Indication
        if (extType != EXT_SNI_CODE) {    
            data += length;
            i += length;

            continue;
        }

        //if we would step out of boundaries
        if (i + SNI_LIST_LEN_BYTES + SNI_TYPE_BYTES > extLengths) {
            return;
        }
        //moving across List Length nad Type
        data += SNI_LIST_LEN_BYTES + SNI_TYPE_BYTES;
        i += SNI_LIST_LEN_BYTES + SNI_TYPE_BYTES;
        //getting length of SNI name
        first = ++data;
        second = ++data;
        int nameLen = (*(first)<<8)|*(second);
        i += 2;
        char *name = malloc((nameLen+1) * sizeof(char));
        data++;
        i++;
        //copying name to node
        if ((i + nameLen) < extLengths) {
            memcpy(name, data, nameLen+1);
            node->sni = name;
        } 

        return;
    }
}

/**
 * Serves for setting next state so we know that handshake or connection termination is finished. 
 * Closes either on two FINs or on RST flag
 *  
 * @param tcpHeader     given tcp header
 * @param actualState   state of the connection before processing this packet
 */
int getNextTcpFlag(const struct tcphdr *tcpHeader, int actualState) {
    switch (actualState) {
        case SYN:
            if (tcpHeader->syn == 1 && tcpHeader->ack == 0) {
                return SYN_ACK;
            }

            if (tcpHeader->rst == 1) {
                return CLOSED;
            }

            return actualState;            
        case SYN_ACK:
            if (tcpHeader->syn == 1 && tcpHeader->ack == 1) {
                return ACK;
            }

            if (tcpHeader->rst == 1) {
                return CLOSED;
            }

            return actualState;
        case ACK:
            if (tcpHeader->syn == 0 && tcpHeader->ack == 1) {
                return FIN;
            }

            if (tcpHeader->rst == 1) {
                return CLOSED;
            }

            return actualState;
        case FIN:
            if (tcpHeader->fin == 1) {
                return FIN2;
            }

            if (tcpHeader->rst == 1) {
                return CLOSED;
            }

            return actualState;
        case FIN2:
            if (tcpHeader->fin == 1) {
                return CLOSED;
            }

            if (tcpHeader->rst == 1) {
                return CLOSED;
            }

            return actualState;
        default:
            return actualState;
    }
}

/**
 * Gets next TLS flag so we know that TLS handshake is finished (awaiting app_data on finish)
 */
int getNextTlsFlag(u_char *data, int actualState)  {
    switch (actualState) { 
        case CLIENT_HELLO: 
            if (*data == TLS_CLIENT_HELLO) {
                return SERVER_HELLO;
            }
            return actualState;
        case SERVER_HELLO:
            if (*data == TLS_SERVER_HELLO) {
                return APP_DATA;
            }
            return actualState;
        case APP_DATA:
            return APP_DATA;
        default:
            return actualState;
    }   
}

/**
 * Removes node from linked list of structs
 * 
 * @param struct SnifferStruct node - node to delete from list
 */
void removeNode(struct SnifferStruct *node) {
    structCount--;
    struct SnifferStruct *head = sslStructs;
    if (isSameConnection(node->srcPort, node->dstPort, node->srcIp, node->dstIp, head)) {
        sslStructs = node->next;
        free(node); 

        return;
    }

    while (head->next != NULL) {
        if (isSameConnection(node->srcPort, node->dstPort, node->srcIp, node->dstIp, head->next)) {
            head->next = node->next;
            free(node);

            return;
        }   

        head = head->next; 
    }
}
 
/**
 * Gets ssl node by src port, dst port, srcIP and dstIp
 * 
 *  @return struct SnifferStruct node on find or NULL if not found
 */
struct SnifferStruct* getSslNode(u_int srcPort, u_int dstPort, char *srcIp, char *dstIp) {
        struct SnifferStruct *node = sslStructs;    
        while (node != NULL) {
            if (isSameConnection(srcPort, dstPort, srcIp, dstIp, node)) {
                return node;
            }
            
            node = node->next;
        }

    return NULL;
}

/**
 * Helper function to find out if two connections are the same based on ports and IP addresses
 * 
 * @param srcPort   source port of new connection
 * @param dstPort   destination port of new connection
 * @param srcIp     source IP of new connection
 * @param dstIp     destination IP of new connection
 * @param node      SnifferStruct node to test against
 */
bool isSameConnection(u_int srcPort, u_int dstPort, char *srcIp, char *dstIp, struct SnifferStruct *node) {
    if (((srcPort == node->srcPort && dstPort == node->dstPort) ||
        (srcPort == node->dstPort && dstPort == node->srcPort)) &&
        ((strcmp(srcIp, node->srcIp) == 0 && strcmp(dstIp, node->dstIp) == 0) || 
        (strcmp(srcIp, node->dstIp) == 0 && strcmp(dstIp, node->srcIp) == 0))) {
            return true;
        }

    return false;
}

/**
 * Gets the last node from linked list
 */
struct SnifferStruct* getLastNode() {
    struct SnifferStruct *node = sslStructs;
    if (node == NULL) {
        return NULL;
    }

    while (node->next != NULL) {
        node = node->next;
    }

    return node;
}

/**
 * Inserts new node to linked list
 * @param new   node to be inserted
 */
void insertSslNode (struct SnifferStruct *new) {
    struct SnifferStruct *node = getLastNode();
    structCount++;
    if (node == NULL) {
        sslStructs = new;
        sslStructs->next = NULL;

        return;
    }

    node->next = new;
    node = node->next;
    node->next = NULL;
}

/**
 * Initializes linked list head pointer
 */
void initializeSslStruct() {
    sslStructs = NULL;
}

/**
 * Sniffs when a an -r option with a name of a file is given
 * 
 * @param filename  Name of file to sniff
 * 
 * @return void
 */
void analyzeFromFile(char* filename) {
    pcap_t* filePointer;
    char errorBuffer[PCAP_ERRBUF_SIZE];

    filePointer = pcap_open_offline(filename, errorBuffer);
    if (filePointer == NULL) {
        fprintf(stderr, "\npcap_open_offline() failed: %s\n", errorBuffer);
	    return;
    }

    if (pcap_loop(filePointer, 0, pcapHandler, NULL) < 0) {
        fprintf(stderr, "\npcap_loop() failed: %s\n", pcap_geterr(filePointer));
        return;
    }

    freeStructs();
}

/**
 * Sniffs when a an -i option with a name of an interface is given
 * 
 * @param interface Name of file to sniff
 * 
 * @return void
 */
void analyzeLive(char* interface) {
    pcap_t* handle;
    char errorBuffer[PCAP_ERRBUF_SIZE];

    handle = pcap_open_live(interface, 65535, 1, 1000, errorBuffer);
    if (handle == NULL) {
        fprintf(stderr, "\npcap_open_live() failed: %s\n", errorBuffer);
	    return;
    }

      if (pcap_loop(handle, 0, pcapHandler, NULL) < 0) {
        fprintf(stderr, "\npcap_loop() failed: %s\n", pcap_geterr(handle));
        return;
    }
}

/**
 * Frees allocated memory
 */
void freeStructs() {
    struct SnifferStruct* node = sslStructs;
    if (node == NULL) {
        return;
    }

    while(node->next != NULL) {
        struct SnifferStruct* next = node->next;
        free(node);
        node = next;
    }
    sslStructs = NULL;
}

/**
 * Entry point 
 */
int main(int argc, char *argv[])
{
    
    bool set = false;
    char* fileName = "";
    char* interface = "";
    handleArguments(argc, argv, &fileName, &interface, &set);

    if (strcmp(fileName, "")) {
        analyzeFromFile(fileName);
    } else if (strcmp(interface, "")) {
        analyzeLive(interface);
    }
    
    return (0);
}